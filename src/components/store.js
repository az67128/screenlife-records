import { writable, get } from 'svelte/store'

export const isLoadingList = writable(true)

export const list = writable([])
export const sortDirection = writable('date')
export const getList = async () => {
  isLoadingList.set(true)
  const res = await fetch(`https://svp-api-v1.azurewebsites.net/api/recommendations/${get(sortDirection)}/?count=100`, {
    headers: { Authorization: '19079d4c-80c8-41ce-aeb6-169a7f4b6b37' },
  })
  const json = await res.json()
  list.set(json)
  isLoadingList.set(false)
}

export const changeSort = (direction) => {
  if (get(sortDirection) === direction) return
  sortDirection.set(direction)
  getList()
}

export const isLoadingBanners = writable(true)

export const banners = writable([])

export const getBanners = async () => {
  isLoadingBanners.set(true)
  const res = await fetch('https://svp-api-v1.azurewebsites.net/api/banners/', {
    headers: { Authorization: '19079d4c-80c8-41ce-aeb6-169a7f4b6b37' },
  })
  const json = await res.json()
  banners.set(json)
  isLoadingBanners.set(false)
}

export const search = writable('')

export const isLoadingSearch = writable(false)

export const makeSearch = async (query) => {
  let currentSearch = query;
  if (!query) {
    searchList.set([])
    isLoadingSearch.set(false)
    return
  }
  
  isLoadingSearch.set(true)
  try {
    const res = await fetch(`https://svp-api-v1.azurewebsites.net/api/search/${query}`, {
      headers: { Authorization: '19079d4c-80c8-41ce-aeb6-169a7f4b6b37' },
    })

    const json = await res.json()
    if(currentSearch !== get(search)){
      return;
    }
    searchList.set(json)
  } catch (err) {}
  
  isLoadingSearch.set(false)
}

export const searchList = writable([])
let searchTimeout;
search.subscribe((state) => {
  clearTimeout(searchTimeout);
  setTimeout(()=>makeSearch(state), 300)
  
})
